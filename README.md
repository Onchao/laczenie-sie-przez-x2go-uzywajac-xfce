---

## Wykonana praca

1. Stworzyliśmy serwer z archlinuxem na którym zainstalowaliśmy x2goserver oraz xfce.
	IP serwera: 138.68.111.87\\
	
2. Połączyliśmy się z serwerem za pomocą x2goclient nie doświadczając żadnych problemów.

3. Połączyliśmy się lokalnie między dwoma komputerami z archlinuxem. Problem czarnego ekranu wystąpił tylko, gdy łączyliśmy się do komputera z włacząnym xfce na tym samym koncie.

4. Problem można rozwiązać używając opcji Custom desktop z poleceniem "dbus-launch startxfce4" lub "Connection to local desktop" jeśli chcemy dzielić ten sam pulpit.

---